module.exports = {
    getUrlComponent: (urlStr) => {
        if (typeof (urlStr) != "string") return null;
        const url = new URL(urlStr);

        let protocol = url.protocol.replace(":", "");
        if (protocol == "https") protocol = "http";

        return {
            href: url.href,
            protocol: protocol,
            hostname: url.host,
            path: url.pathname,
        }
    }
}