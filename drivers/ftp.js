const Client = require('ftp');
const fs = require('fs');

module.exports = {
    download: (urlComponent, options, onUpdate) => {
        if (!options.profile) options.profile = {};
        const client = new Client();
        client.connect({
            host: urlComponent.hostname,
            user: options.profile.username || 'anonymous',
            password: options.profile.password || 'anonymous@'
        });
        client.on('ready', () => {
            onUpdate('downloading', options.ref)
            client.get(urlComponent.path, (err, stream) => {
                if (err) onUpdate('fail', options.ref, err);
                else {
                    stream.once('close', () => { client.end(); });
                    stream.pipe(fs.createWriteStream(`${options.dest}/${options.savedName}`))
                        .on('finish', () => onUpdate('finish', options.ref))
                        .on('error', (err2) => onUpdate('fail', options.ref, err2));
                }
            });
        });
        client.on('error', (err) => onUpdate('fail', options.ref, err));
    }
}