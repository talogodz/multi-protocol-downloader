const path = require('path');
const fs = require('fs');
const utils = require('./utils');
let supportedProtocol = [];
let drivers = [];
fs.readdirSync(path.join(__dirname, '/drivers')).forEach(file => {
    const [name] = file.split('.');
    drivers[name] = require(`./drivers/${file}`);
    supportedProtocol.push(name);
});

module.exports = {
    download: (url, options, onUpdate) => {
        const urlComponent = utils.getUrlComponent(url);
        if (!supportedProtocol.includes(urlComponent.protocol)) throw new Error('protocol not supported');
        drivers[urlComponent.protocol].download(urlComponent, options, onUpdate);
    }
}