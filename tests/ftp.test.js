const events = require("events");
const ftpDriver = require('../drivers/ftp');
const ftp = require('ftp');
const fs = require('fs');
jest.mock('ftp');
jest.mock('fs');
fs.createWriteStream.mockImplementation((path) => { });

const ftpServerTemplate = (user, pass, interrupted = false) => {
    return () => {
        const hostname = 'mock.ftp.server';
        const ftpUser = user;
        const ftpPass = pass;
        const validPath = '/valid/path';

        const self = new events.EventEmitter();
        const response = new events.EventEmitter();
        response.pipe = jest.fn().mockImplementation(() => response);

        self.get = jest.fn().mockImplementation((path, callback) => {
            if (path != validPath) {
                setInterval(() => response.emit('error', 'path invalid'), 1);
            } else {
                if (interrupted) {
                    setInterval(() => response.emit('error', 'streaming interrupted'), 1);
                } else {
                    setInterval(() => response.emit('finish'), 1);
                }
            }
            callback(null, response);
        })

        self.connect = jest.fn().mockImplementation((options) => {
            if (options.user == ftpUser && options.password == ftpPass && options.host == hostname)
                setInterval(() => self.emit('ready'), 10);
            else
                setInterval(() => self.emit('error', 'error credentials'), 20);
        });

        return self;
    }
}

test('valid path from public ftp server should result a finish status', (done => {
    ftp.mockImplementationOnce(ftpServerTemplate('anonymous', 'anonymous@')); //pubic ftp server;
    ftpDriver.download({ hostname: 'mock.ftp.server', path: '/valid/path' },
        {
            dest: '/destmock', savedName: 'mockfile.png', ref: 'mock-ref',
            profile: {}
        },
        (status, ref, err) => {
            if (err) done(err);
            if (status == 'finish') { done() }
        }
    );
}));


test('*valid* credential should result a finish status', (done => {
    ftp.mockImplementationOnce(ftpServerTemplate('private', 'password')); //private ftp server;

    ftpDriver.download({ hostname: 'mock.ftp.server', path: '/valid/path' },
        {
            dest: '/destmock', savedName: 'mockfile.png', ref: 'mock-ref',
            profile: { username: 'private', password: 'password' }
        },
        (status, ref, err) => {
            if (err) done(err);
            else done()
        }
    );
}));


test('*invalid* credential should result a fail status', (done => {
    ftp.mockImplementationOnce(ftpServerTemplate('private', 'password')); //private ftp server;

    ftpDriver.download({ hostname: 'mock.ftp.server', path: '/valid/path' },
        {
            dest: '/destmock', savedName: 'mockfile.png', ref: 'mock-ref',
            profile: { username: 'invalid-username', password: 'password' }
        },
        (status, ref, err) => {
            if (err) done();
            else if (status == 'finish') done('should return fail')
        }
    );
}));

test('*invalid* file path should result a fail status', (done => {
    ftp.mockImplementationOnce(ftpServerTemplate('anonymous', 'anonymous@')); //pubic ftp server;
    ftpDriver.download({ hostname: 'mock.ftp.server', path: '/invalid-path' },
        {
            dest: '/destmock', savedName: 'mockfile.png', ref: 'mock-ref',
            profile: {}
        },
        (status, ref, err) => {
            if (err) done();
            else if (status == 'finish') done('should return fail')
        }
    );
}));

test('*interrupted* file streaiming should result a fail status', (done => {
    ftp.mockImplementationOnce(ftpServerTemplate('anonymous', 'anonymous@', true));
    ftpDriver.download({ hostname: 'mock.ftp.server', path: '/valid/path' },
        {
            dest: '/destmock', savedName: 'mockfile.png', ref: 'mock-ref',
            profile: {}
        },
        (status, ref, err) => {
            if (err) done();
            else if (status == 'finish') done('should return fail')
        }
    );
}));

