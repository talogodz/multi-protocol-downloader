const exec = require('child_process').exec;
const fs = require('fs');

const multiProtocolDownloader = require('../index');

beforeEach(() => {
    try {
        exec("mkdir downloads");
    } catch (_) { }

});

afterEach(() => {
    try {
        exec("rm -rf downloads");
    } catch (_) { }
});

test('download file from http', (done) => {
    multiProtocolDownloader.download(
        'https://homepages.cae.wisc.edu/~ece533/images/airplane.png',
        {
            savedName: "airplane.png",
            dest: "./downloads"
        }
        , (status, ref, err) => {
            if (err) done(err);
            else if (status == 'finish') {
                fs.readFileSync(`./downloads/airplane.png`);
                done();
            }
        }
    )
});

test('download file from ftp', (done) => {
    multiProtocolDownloader.download(
        'ftp://test.rebex.net/pub/example/KeyGenerator.png',
        {
            savedName: "KeyGenerator.png",
            dest: "./downloads",
            profile: {
                username: "demo",
                password: "password"
            }
        }
        , (status, ref, err) => {
            if (err) done(err);
            else if (status == 'finish') {
                fs.readFileSync(`./downloads/KeyGenerator.png`);
                done();
            }
        }
    )
});


test('download file from sftp', (done) => {
    multiProtocolDownloader.download(
        'sftp://test.rebex.net/readme.txt',
        {
            savedName: "readme.txt",
            dest: "./downloads",
            profile: {
                username: "demo",
                password: "password"
            }
        }
        , (status, ref, err) => {
            if (err) done(err);
            else if (status == 'finish') {
                fs.readFileSync(`./downloads/readme.txt`);
                done();
            }
        }
    )
});

test('while download large file should not leave any partial files in destination folder', (done) => {
    multiProtocolDownloader.download(
        'https://homepages.cae.wisc.edu/~ece533/images/airplane.png',
        {
            savedName: "airplane.png",
            dest: "./downloads"
        }
        , (status, ref, err) => {
            if (err) done(err);
        });

    setTimeout(() => {
        const filenames = fs.readdirSync('./downloads');
        if (!filenames.length) {
            done();
        }
        else {
            done('there is a file in destination folder');
        }
    }, 300)
});