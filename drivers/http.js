const fs = require('fs');
const request = require('request');

module.exports = {
    download: (urlComponent, options, onUpdate) => {
        onUpdate('downloading', options.ref);
        request(urlComponent.href)
            .on('response', (response) => {
                response.pipe(fs.createWriteStream(`${options.dest}/${options.savedName}`))
                    .on('finish', () => onUpdate('finish', options.ref))
                    .on('error', (err) => onUpdate('fail', options.ref, err));
            })
            .on('error', (err) => onUpdate('fail', options.ref, err));
    }
}