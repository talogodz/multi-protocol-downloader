const events = require("events");
const httpDriver = require('../drivers/http');
const request = require('request');
const fs = require('fs');
jest.mock('request');
jest.mock('fs');
fs.createWriteStream.mockImplementation((path) => { });

test('download file from valid http url should return finish status', (done => {
    // mock request which return successful stream data
    request.mockImplementationOnce(params => {
        const self = new events.EventEmitter();
        const response = new events.EventEmitter();

        response.pipe = jest.fn().mockImplementation(() => response);
        setInterval(() => response.emit('finish', 'finish-test'), 10);
        setInterval(() => self.emit('response', response), 10);

        return self;
    })

    httpDriver.download({ href: 'some mock url' },
        { dest: 'mock dest', savedName: 'mockfile.png', ref: 'mock-ref' },
        (status, ref, err) => {
            if (err) done(err);
            if (status == 'finish') { done() }
        }
    );
}));

test('download file from invalid return fail status', (done => {
    // mock request which return error event
    request.mockImplementationOnce(params => {
        const self = new events.EventEmitter();
        setInterval(() => self.emit('error', new Error('404')), 10);
        return self;
    })

    httpDriver.download({ href: 'some mock url' },
        { dest: 'mock dest', savedName: 'mockfile.png', ref: 'mock-ref' },
        (status, ref, err) => {
            if (status == 'fail' && err) done();
            else if (status != 'downloading') done('must return error with status fail')
        }
    );
}));


test('download file from valid http but streaming is interrupted should return fail status with error', (done => {
    // mock request which return error while stream data
    request.mockImplementationOnce(params => {
        const self = new events.EventEmitter();
        const response = new events.EventEmitter();

        response.pipe = jest.fn().mockImplementation(() => response);
        setInterval(() => response.emit('error', new Error('streaming interrupted')), 20);
        setInterval(() => self.emit('response', response), 10);

        return self;
    })

    httpDriver.download({ href: 'some mock url' },
        { dest: 'mock dest', savedName: 'mockfile.png', ref: 'mock-ref' },
        (status, ref, err) => {
            if (status == 'fail' && err) done();
            else if (status != 'downloading') done('must return error with status fail')
        }
    );

}));
