# MULTI-PROTOCOL-DOWNLOADER

## install

```bash
npm install multi-protocol-downloader -S
```


## test

```bash
npm i jest -D
npm run test

```

## usage

```js
const mpd = require('multi-protocol-downloader');

mpd.download(
    'https://homepages.cae.wisc.edu/~ece533/images/airplane.png',
    {
        savedName: "airplane.png",
        dest: "./downloads",  //folder must be exist
    }
    , (status, ref, err) => {
        console.log(status);
    }
);
```
