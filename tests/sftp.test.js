const events = require("events");
const sftpDriver = require('../drivers/sftp');
const sftp = require('ssh2-sftp-client');
const fs = require('fs');
jest.mock('ssh2-sftp-client');
jest.mock('fs');
fs.createWriteStream.mockImplementation((path) => { });

const sftpServerTemplate = (user, pass, interrupted = false) => {
    return () => {
        const hostname = 'mock.sftp.server';
        const sftpUser = user;
        const sftpPass = pass;
        const validPath = '/valid/path';

        const self = new events.EventEmitter();

        self.get = jest.fn().mockImplementation((path, stream) => {
            if (path != validPath) {
                return Promise.resolve({});
            } else {
                if (interrupted) {
                    return Promise.reject('interrupted streaming');
                } else {
                    return Promise.resolve({});
                }
            }
        })
        self.end = jest.fn().mockImplementation(() => { });

        self.connect = jest.fn().mockImplementation((options) => {
            if (options.username == sftpUser && options.password == sftpPass && options.host == hostname) {
                return Promise.resolve({});
            }
            else
                return Promise.reject('invalid credentials');
        });

        return self;
    }
}

test('valid sftp server path should result a finish status', (done => {
    sftp.mockImplementationOnce(sftpServerTemplate('validuser', 'password'));
    sftpDriver.download({ hostname: 'mock.sftp.server', path: '/valid/path' },
        {
            dest: '/destmock', savedName: 'mockfile.png', ref: 'mock-ref',
            profile: { username: 'validuser', password: 'password' }
        },
        (status, ref, err) => {
            if (err) done(err);
            if (status == 'finish') { done() }
        }
    );
}));


test('*invalid* user cred. should result a fail status', (done => {
    sftp.mockImplementationOnce(sftpServerTemplate('validuser', 'password'));
    sftpDriver.download({ hostname: 'mock.sftp.server', path: '/valid/path' },
        {
            dest: '/destmock', savedName: 'mockfile.png', ref: 'mock-ref',
            profile: { username: 'invalid', password: 'invalid' }
        },
        (status, ref, err) => {
            if (err && status == 'fail') done();
            else done('should return fail')
        }
    );
}));


test('*interrupted* streaiming file should result a fail status', (done => {
    sftp.mockImplementationOnce(sftpServerTemplate('validuser', 'password', true));
    sftpDriver.download({ hostname: 'mock.sftp.server', path: '/valid/path' },
        {
            dest: '/destmock', savedName: 'mockfile.png', ref: 'mock-ref',
            profile: { username: 'validuser', password: 'password' }
        },
        (status, ref, err) => {
            if (err && status == 'fail') done();
            else if (status == 'finish') done('should return fail');
        }
    );
}));
