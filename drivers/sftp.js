const Client = require('ssh2-sftp-client');
const fs = require('fs');

module.exports = {
    download: (urlComponent, options, onUpdate) => {
        const sftp = new Client();
        sftp.connect({
            host: urlComponent.hostname,
            username: options.profile.username,
            password: options.profile.password
        }).then(() => {
            onUpdate('downloading', urlComponent.ref)
            sftp.get(urlComponent.path, fs.createWriteStream(`${options.dest}/${options.savedName}`))
                .then(() => {
                    onUpdate('finish', options.ref)
                    sftp.end();
                }).catch((err) => onUpdate('fail', options.ref, err));
        }).catch((err) => onUpdate('fail', options.ref, err));
    }
}